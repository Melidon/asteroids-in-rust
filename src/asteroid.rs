use raylib::prelude::*;

#[derive(Clone, Copy)]
pub enum AsteroidSize {
    Small = 1,
    Medium = 2,
    Large = 4,
}

pub struct Asteroid {
    size: AsteroidSize,

    position: Vector2,
    velocity: Vector2,

    rotation: f32,
    rotation_speed: f32,
}

const ASTEROID_ROTATION_SPEED_MAXIMUM: i32 = 128;

impl Asteroid {
    pub fn new(size: AsteroidSize, position: Vector2, velocity: Vector2) -> Self {
        Asteroid {
            size,
            position,
            velocity,
            rotation: get_random_value::<i32>(0, 360) as f32,
            rotation_speed: get_random_value::<i32>(
                -ASTEROID_ROTATION_SPEED_MAXIMUM,
                ASTEROID_ROTATION_SPEED_MAXIMUM,
            ) as f32,
        }
    }

    pub fn is_off_screen(&self, screen_width: i32, screen_height: i32) -> bool {
        self.position.x < 0.0
            || self.position.x > screen_width as f32
            || self.position.y < 0.0
            || self.position.y > screen_height as f32
    }

    pub fn update(&mut self, frame_time: f32) {
        self.position += self.velocity * frame_time;
        self.rotation += self.rotation_speed * frame_time;
    }

    pub fn draw(&self, draw_handle: &mut RaylibDrawHandle) {
        draw_handle.draw_poly_lines(
            self.position,
            3,
            16.0 * self.size as u8 as f32,
            self.rotation,
            Color::WHITE,
        );
    }
}
