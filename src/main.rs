mod asteroid;

use asteroid::{Asteroid, AsteroidSize};
use raylib::prelude::*;

const SCREEN_WIDTH: i32 = 720;
const SCREEN_HEIGHT: i32 = 720;

fn main() {
    let mut asteroids = Vec::new();
    let (mut handle, thread) = raylib::init()
        .size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .title("Asteroids")
        .build();
    while !handle.window_should_close() {
        update(&handle, &mut asteroids);
        draw(&mut handle, &thread, &asteroids);
    }
}

fn update(handle: &RaylibHandle, asteroids: &mut Vec<Asteroid>) {
    // Update asteroids
    let frame_time = handle.get_frame_time();
    for asteroid in asteroids.iter_mut() {
        asteroid.update(frame_time);
    }
    // Remove asteroids that are off the screen
    asteroids.retain(|asteroid| !asteroid.is_off_screen(SCREEN_WIDTH, SCREEN_HEIGHT));
    // Handle user input
    if handle.is_mouse_button_pressed(MouseButton::MOUSE_LEFT_BUTTON) {
        let mouse_position = handle.get_mouse_position();
        let random_size = match get_random_value::<i32>(1, 3) {
            1 => AsteroidSize::Small,
            2 => AsteroidSize::Medium,
            3 => AsteroidSize::Large,
            _ => unreachable!(),
        };
        asteroids.push(Asteroid::new(
            random_size,
            mouse_position,
            Vector2::new(128.0, 0.0),
        ));
    }
}

fn draw(handle: &mut RaylibHandle, thread: &RaylibThread, asteroids: &[Asteroid]) {
    let mut draw_handle = handle.begin_drawing(thread);
    draw_handle.clear_background(Color::BLACK);
    for asteroid in asteroids.iter() {
        asteroid.draw(&mut draw_handle);
    }
}
